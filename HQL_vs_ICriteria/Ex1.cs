﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Reflection;

namespace HQL_vs_ICriteria
{
    class Ex1
    {
        static void Main(string[] args)
        {
            Configuration configuration = new Configuration();
            configuration.AddAssembly(Assembly.GetCallingAssembly());
            ISessionFactory sessionFactory = configuration.BuildSessionFactory();
            ISession session = sessionFactory.OpenSession();

            //TO DO: fetch data via HQL
            //TO DO: fetch data via ICriteria

        }
    }

    class SampleTable
    {
        public virtual string RetailerCountry { get; set; }
        public virtual string OrderMethodType { get; set; }
        public virtual string RetailerType { get; set; }
        public virtual string ProductLine { get; set; }
        public virtual string ProductType { get; set; }
        public virtual string Product { get; set; }
        public virtual int Year { get; set; }
        public virtual string Quarter { get; set; }
        public virtual float Revenue { get; set; }
        public virtual int Quantity { get; set; }
        public virtual float GrossMargin { get; set; }
    }
}
